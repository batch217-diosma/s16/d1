console.log("hello");

// aarithmeticc operators

let x = 3;
let y = 10;

let sum = x + y ;
console.log("sum: " + sum);

let difference = x - y ;
console.log("difference: " + difference);

let product = x * y ;
console.log("product: " + product);

let quotient = x / y ;
console.log("quotient: " + quotient);

let remainder = y % x ;
console.log("remainder: " + remainder);

// aassignment operators

    // basic assignment operator

    let assignmentNumber  = 8;

    assignmentNumber = assignmentNumber + 2;
    console.log("sum: " + assignmentNumber);
          //shorttcut
    assignmentNumber += 2;
    console.log("sum: " + assignmentNumber);

    assignmentNumber -= 2;
    console.log("difference: " + assignmentNumber);

    assignmentNumber *= 2;
    console.log("product: " + assignmentNumber);

    assignmentNumber /= 2;
    console.log("quotient: " + assignmentNumber);

// mmultiple operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5 ;
console.log("answer: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5) ;
console.log("answer: " + pemdas);

// incrementation and decremaentation
// pre-increment
let z =  1 ;
let increment = ++z ;
console.log("result off pre-incremeent: " + increment);

// post-increment
increment = z++ ;
//increment = z++ ;
console.log("result off post-incremeent: " + increment);

// pre-decrement
//let decrement = --z ;
let decrement = --z ;
console.log("result off pre-decrement: " + decrement);

// post-decrement
decrement = z-- ;
console.log("result off post-decrement: " + decrement);

// type coercion

let numA = '10';
let numB = 12;
let coercion = numA + numB ;
console.log("answer: " + coercion);
console.log(typeof coercion);

let numC = 16 ;
let numD = 14 ;
let nonCoercion = numC + numD ;
console.log("answer: " + nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1 ;
console.log("answer: " +  numE);

let numF = false + 1 ;
console.log("answer: " +  numF);

// comparison operators 
let juan = "juan" ;

// eqquality operator (==)

console.log(1==1) ;
console.log(1==2) ;
console.log(1=="1") ;
console.log(0==false) ;
console.log("juan" == "juan") ;
console.log("juan" == juan) ;

//inequality operator (!=)

console.log("juan" != "juan") ;
console.log(1!=1) ;
console.log(1!=2) ;

// sstrict equality opereator (===)

console.log(1==='1') ;
console.log(1===1) ;
console.log("juan" === juan) ;
console.log(0===false) ;

// strict inequality operator

console.log(1!=='1') ;
console.log(1!==1) ;
console.log("juan" !== juan) ;
console.log(1!==false) ;

// relational operator (> , < , =)

let  a = 50 ;
let b = 65 ;
let isGreaterThan = a > b ;
console.log(isGreaterThan);

let isLessThan = a < b ;
console.log(isLessThan);

let isGTorEqual = a >= b ;
console.log(isGTorEqual);

let isLTorEqual = a <= b ;
console.log(isLTorEqual);

